<html>
<head>
    <title>Read Data</title>
    <link href="<?php echo base_url('assets/bootstrap-3.3.5-dist/css/bootstrap.css')?>" type="text/css" rel="stylesheet"/>
</head>
<body>

<?php if($this->session->flashdata('item')) { ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('item'); ?>
    </div>
<?php }else if($this->session->flashdata('invalid')){
    ?>
    <div class="alert alert-danger">
        <?php echo $this->session->flashdata('invalid'); ?>
    </div>
<?php
} ?>
<h3 class="text-center">View Data From Postgresql</h3>
<table class="table table-striped">
    <tr>
    <th>No</th>
    <th>id_user</th>
    <th>nama</th>
    <th>alamat</th>
     <th>Action</th>
    </tr>
    <?php
    $a=1;
    foreach($result as $value){
        ?>
        <tr>
            <td><?=$a?></td>
            <td><?=$value->id_user?></td>
            <td><?=$value->nama?></td>
            <td><?=$value->alamat?></td>
            <td><a href="<?=base_url('welcome/update/'.$value->id_user);?>" class="btn btn-info">Update</a> <a href="<?=base_url('welcome/delete/'.$value->id_user);?>" class="btn btn-danger">Delete</a></td>
        </tr>
    <?php
        $a++;
    }
    ?>
</table>
<div class="col-md-12 text-left">
    <a href="<?=base_url('welcome/create')?>" class="btn btn-primary">Create</a>
</div>
</body>
</html>